---
title: Topics
media_order: 'compliance.jpg,differential-kinematics.jpg,rigid-body-dynamics.jpg,screw-theory.jpg,lie-group-theory.png,type-synthesis.png'
class: standard
features:
    -
        avatar: screw-theory.jpg
        header: 'Screw Theory'
        text: 'Basic vector-space properties of twists and wrenches: physical interpretation of the linear operations; linear dependence and independence, subspaces; bases and coordinates. Screw systems: geometry and classification, invariance and persistance. (Lecturers: Dimiter Zlatanov and Marco Carricato) '
    -
        avatar: type-synthesis.png
        header: 'Type Synthesis'
        text: 'Scalar products, dual spaces, reciprocity. Constraint and freedom in mechanisms. Constraint analysis. Type synthesis of single-loop mechanisms and parallel manipulators. (Lecturers: Xianwen Kong and Dimiter Zlatanov)'
    -
        avatar: differential-kinematics.jpg
        header: 'Differential Kinematics'
        text: 'Velocity and singularity analysis of parallel and interconnected-chain mechanisms. Derivation of input-output velocity equations and singularity conditions. (Lecturers: Matteo Zoppi and Dimiter Zlatanov)'
    -
        avatar: compliance.jpg
        header: Compliance
        text: 'Mappings between screw spaces, stiffness and inertia. Structure of robot compliance. Eigenvalue problems and eigenscrews. Synthesis with springs. (Lecturer: Harvey Lipkin)'
    -
        avatar: rigid-body-dynamics.jpg
        header: 'Rigid Body Dynamics'
        text: '6D formulation of the dynamics of individual rigid bodies and rigid-body systems. Equations of motion. Dynamics algorithms. (Lecturer: Roy Featherstone)'
    -
        avatar: lie-group-theory.png
        header: 'Lie Group Theory'
        text: 'Basic Lie group theory, matrix representations of the group of rigid-body displacements.Lie algebras as related to screw theory. The exponential map andits applications in modern robotics (Lecturer: Jon Selig). '
---

## Topics


There are many topics where screw theory based methods are applied in robotics: