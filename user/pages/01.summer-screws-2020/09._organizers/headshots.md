---
title: Organizers
media_order: 'ayassir.jpg,fbroeren.jpg,jrommers.jpg,ptempel.jpg,vvanderwijk.jpg'
class: standard
lecturers:
    -
        avatar: vvanderwijk.jpg
        header: 'Volkert van der Wijk'
    -
        avatar: ayassir.jpg
        header: 'Abdullah Yaşır'
    -
        avatar: fbroeren.jpg
        header: 'Freek Broeren'
    -
        avatar: ptempel.jpg
        header: 'Philipp Tempel'
    -
        header: 'Ad Huisjes'
    -
        avatar: jrommers.jpg
        header: 'Jelle Rommers'
image_align: left
---

## Organizing Committee

Summer Screws 2020 is organized by the group of Mechatronic Systems Design at the Department of Precision and Microsystems Engineering of the Delft University of Technology, Delft, The Netherlands.

#### Address:
Delft University of Technology  
Faculty of Mechanical, Maritime, and Materials Engineering  
Department of Precision and Microsystems Engineering  
Mekelweg 2, 2628 CD Delft  
The Netherlands

To contact the organizers, send an email to screws@summerscrews2020.org