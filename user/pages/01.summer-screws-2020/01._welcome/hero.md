---
title: Welcome
media_order: 'ben-o-bro-wpU4veNGnHg-unsplash.jpg,Picture1.jpg,20140611_tudelft_campus_R9B0532.jpeg,TU_zijbalk.jpg'
hero_classes: 'parallax hero-fullscreen overlay-dark-gradient text-light title-h1h2'
hero_image: 20140611_tudelft_campus_R9B0532.jpeg
hidemenu: true
---

# 9th International Summer School on
## Screw-Theory Based Methods in Robotics

### <strike>July 25 &ndash; August 2, 2020</strike>
### Postponed &mdash; Information will follow
#### Delft University of Technology, Delft, The Netherlands
